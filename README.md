#################Running the code#################

sudo docker-compose -f eatfreshfood.yaml up

##################################################

-----------------Generating Keys for Agent(Agent) and Distributer(Distri)------------------
*****************Login to client container by:*******************
sudo docker exec -it eatfreshfood_client bash
*****************Generate and view keys by following commands******************* 
1)sawtooth keygen Agent
2)cat ~/.sawtooth/keys/Agent.priv
3)sawtooth keygen Distri
4)cat ~/.sawtooth/keys/Distri.priv

Html page:
1)  Go to tab for farmer service agency.
    Enter the details.
    Confirm identity with user id "Agent" and pvt key.
2)  Go to tab for Distributer.
    Enter the details.
    Confirm identity with user id "Distri" and pvt key.
3)  Go to tab called Track product details.
    Enter the product id.
    Click button to view the details.
4)  Go to tab called Discover origin.
    Enter the product id.
    Click button to see the origin place of the product.



Note: Map works fine even though google map shows error since billing info hasn't been added since it is for development purpose. 


