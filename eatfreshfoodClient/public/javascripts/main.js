
function viewData() {
    window.location.href='/listView';

}

function addEatfreshAsDistrib(event) {
    // write your logic here ^_^
    event.preventDefault();
    const data1 = document.getElementById('product_idDis').value
    const data2 = document.getElementById('obtimestamp').value
    const data3 = document.getElementById('droptimestamp').value
    const data4 = document.getElementById('uidD').value
    const data5 = document.getElementById('pvtkeyDis').value
    const data='D '+data1+' '+data2+' '+data3;
    console.log(data);
    if(data1==''||data2==''||data3==''||data4==''){
        alert("please fill in all the details")
    }
    else{
        $.post('/', {  write: data , pid: data1 , pvt: data5, sender: data4 , role: 'D' },
        function (data, textStatus, jqXHR) {
            alert(data.message);
        },
        'json');
        }
    
}

function addEatfreshAsAgent(event) {

    // write your logic here ^_^
    event.preventDefault();
    const data1 = document.getElementById('product_idAg').value
    const data2 = document.getElementById('product_name').value
    const data3 = document.getElementById('weight').value
    const data4 = document.getElementById('price').value
    const data5 = document.getElementById('dopacking').value
    const data6 = document.getElementById('bestbefore').value
    const data7 = document.getElementById('place').value
    const data8 = document.getElementById('uidA').value
    const data9 = document.getElementById('pvtkeyAg').value

    const data='A '+data1+' '+data2+' '+data3+' Rs.'+data4+' '+data5+' '+data6+' '+data7;
    console.log(data);
    if(data1==''||data2==''||data3==''||data4==''||data5==''||data6==''||data7==''||data8==''||data9==''){
        alert("please fill in all the details")
    }
    else{
        $.post('/', {  write: data , pid: data1 , pvt: data9 , sender: data8 , role: 'A' },
        function (data, textStatus, jqXHR) {
            alert(data.message);
        },
        'json');
        }
}


function readmsgfull(event){
    event.preventDefault();
    const pid = document.getElementById('product_idR').value
    $.get('/state/'+pid,  (data,textStatus,jqXHR) =>{
        // alert("your data is : " + data.balance)
        if(data.balance=='unauthorised'){
        alert("No data found");
        msgArr="notfound ";
        }
        msgStr=data.balance;
        msgArr=msgStr.split(" ");
        console.log('msgArr is '+msgArr);
        document.getElementById("pnme").value = msgArr[2]; 
        document.getElementById("wei").value =msgArr[3]; 
        document.getElementById("pri").value = msgArr[4]; 
        document.getElementById("pack").value = msgArr[5]; 
        document.getElementById("before").value = msgArr[6]; 
        document.getElementById("pickup").value = msgArr[10]; 
        document.getElementById("dropoff").value = msgArr[11];

      },'json');
    
}

function trackfn(event){
    event.preventDefault();
    const pid = document.getElementById('product_idT').value
    $.get('/state/'+pid,  (data,textStatus,jqXHR) =>{
        // alert("your data is : " + data.balance)
        if(data.balance=='unauthorised'){
            alert("No data found")
            msgStr="notfound";
        }
        msgStr=data.balance;
        msgArr=msgStr.split(" ");
        alert("The "+msgArr[2]+" is from " + msgArr[7]);
        console.log('msgArr is '+msgArr[7]);
        if(msgArr[7]=="Kasaragod")
        setNewCenter(12.5102,74.9852);
        if(msgArr[7]=="Kannur")
        setNewCenter(11.8745,75.3704);
        if(msgArr[7]=="Wayanad")
        setNewCenter(11.6854,76.1320);
        if(msgArr[7]=="Kozhikode")
        setNewCenter(11.2588,75.7804);
        if(msgArr[7]=="Malappuram")
        setNewCenter(11.0510,76.0711);
        if(msgArr[7]=="Palakkad")
        setNewCenter(10.7867,76.6548);
        if(msgArr[7]=="Thrissur")
        setNewCenter(10.5276,76.2144);
        if(msgArr[7]=="Ernakulam")
        setNewCenter(9.9816,76.2999);
        if(msgArr[7]=="Idukki")
        setNewCenter(9.9189,77.1025);
        if(msgArr[7]=="Kottayam")
        setNewCenter(9.5916,76.5222);
        if(msgArr[7]=="Alappuzha")
        setNewCenter(9.4981,76.3388);
        if(msgArr[7]=="Pathanamthitta")
        setNewCenter(9.2648,76.7870);
        if(msgArr[7]=="Kollam")
        setNewCenter(8.8932,76.6141);
        if(msgArr[7]=="Thiruvanthapuram")
        setNewCenter(8.5241,76.9366);
        
        

      },'json');
    
}