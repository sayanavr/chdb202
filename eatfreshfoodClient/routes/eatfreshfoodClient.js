const {createHash} = require('crypto')
const {CryptoFactory, createContext } = require('sawtooth-sdk/signing')
const protobuf = require('sawtooth-sdk/protobuf')
const fs = require('fs')
const http = require('http');
const fetch = require('node-fetch');
const {Secp256k1PrivateKey} = require('sawtooth-sdk/signing/secp256k1')	
const {TextEncoder, TextDecoder} = require('text-encoding/lib/encoding')

FAMILY_NAME='eatfreshfood'


function hash(v) {
  return createHash('sha512').update(v).digest('hex');
}

class eatfreshfoodClient{
  constructor(pid,pvt,sndr,role){
    ///////////
    if(sndr=='Read'){
      this.address=hash("eatfreshDb").substr(0,6)+hash("thisisarandomtextusedtojustgeneratetheaddresshereinmyeatfreshcode").substr(0,60)+hash(pid).slice(0,4);
    
    }else
    { 
      if(role=='A')
      {
    const privateKey1StrBuf = this.getUserPriKey(sndr);
    if(privateKey1StrBuf!='noentry')
    {
    const privateKey1Str = privateKey1StrBuf.toString().trim();
    console.log('privateKey1Str is '+privateKey1Str);
    console.log('pvt is '+pvt);
      if(pvt==privateKey1Str)
        {
        const context = createContext('secp256k1');
        const secp256k1pk=Secp256k1PrivateKey.fromHex(privateKey1Str.trim());
        this.signer=new CryptoFactory(context).newSigner(secp256k1pk);
        this.publicKey=this.signer.getPublicKey().asHex();

    this.address=hash("eatfreshDb").substr(0,6)+hash("thisisarandomtextusedtojustgeneratetheaddresshereinmyeatfreshcode").substr(0,60)+hash(pid).slice(0,4);
    console.log("Storing at: " + this.address);
        }
        else{
        console.log('no address gen');
        return {msg:'err'};
        }
      }
      else{
        console.log('no entry in directory');
        return {msg:'err'};
      }
    }
    else if(role=='D')
    {
  const privateKey1StrBuf = this.getUserPriKey(sndr);
  if(privateKey1StrBuf!='noentry')
  {
  const privateKey1Str = privateKey1StrBuf.toString().trim();
  console.log('privateKey1Str is '+privateKey1Str);
  console.log('pvt is '+pvt);
    if(pvt==privateKey1Str)
      {
      const context = createContext('secp256k1');
      const secp256k1pk=Secp256k1PrivateKey.fromHex(privateKey1Str.trim());
      this.signer=new CryptoFactory(context).newSigner(secp256k1pk);
      this.publicKey=this.signer.getPublicKey().asHex();

  this.address=hash("eatfreshDb").substr(0,6)+hash("thisisarandomtextusedtojustgeneratetheaddresshereinmyeatfreshcode").substr(0,60)+hash(pid).slice(0,4);
  console.log("Storing at: " + this.address);
      }
      else{
      console.log('no address gen');
      return {msg:'err'};
      }
    }
    else{
      console.log('no entry in directory');
      return {msg:'err'};
    }
  }
  else{
    console.log('no entry in directory');
    return {msg:'err'};
  }
  }
    
    
  }
  getUserPriKey(userid) {
    console.log(userid);
    console.log("Current working directory is: " + process.cwd());
    var userprivkeyfile = '/root/.sawtooth/keys/'+userid+'.priv';
    
    try{
    return fs.readFileSync(userprivkeyfile);
    }
    catch(err){
      console.log(err);
      return 'noentry';
    }
    
  }	
  send_data(values){
   
    //create payload
    var payload = values;
    const address = this.address;
    console.log('In UserClient :'+values);
    if(this.address==null){
      console.log("illegal access error");
      return 'unauthorised';
    }
    //convert payload into byte format
    var encode =new TextEncoder('utf8');
    const payloadBytes = encode.encode(payload)
    
    //create transaction header in byte format (use protobuf)
    const transactionHeaderBytes = protobuf.TransactionHeader.encode({
      familyName:FAMILY_NAME,
      familyVersion:'1.0',
      inputs: [address],
      outputs: [address],
      signerPublicKey: this.publicKey,
      nonce: "" + Math.random(),
      batcherPublicKey: this.publicKey,
      dependencies: [],
      payloadSha512:hash(payloadBytes)
      }).finish()
    
    //create transaction(use protobuf)
    const transaction = protobuf.Transaction.create({
      header: transactionHeaderBytes,
      headerSignature: this.signer.sign(transactionHeaderBytes),
      payload: payloadBytes
    })
    const transactions = [transaction];
    
    
    //create batch header in byte format(use protobuf)
    
    const batchHeaderBytes = protobuf.BatchHeader.encode({
    signerPublicKey: this.signer.getPublicKey().asHex(),
    transactionIds: transactions.map((txn) => txn.headerSignature),
    }).finish();
    
    //create batch (use protobuf)
    const batchSignature = this.signer.sign(batchHeaderBytes);
     const batch = protobuf.Batch.create({
       header: batchHeaderBytes,
       headerSignature: batchSignature,
       transactions: transactions,
     })
    
    //create batchlist (use protobuf)
    const batchListBytes = protobuf.BatchList.encode({
      batches: [batch]
    }).finish();
    
    
      this._send_to_rest_api(batchListBytes);	
    }
    
    
    async _send_to_rest_api(batchListBytes){
      if (batchListBytes == null) {
        try{
    
      //fetch  data from state by using node-fetch module
      var geturl = 'http://rest-api:8008/state/'+this.address
      let response=await fetch(geturl, {
        method: 'GET',
      })
      let responseJson = await response.json();
        var data = responseJson.data;
        var newdata = new Buffer(data, 'base64').toString();
        return newdata;
    
        }
        catch(error) {
          console.error(error);
          return 'unauthorised';
        }	
      }
      else{
        try{
             
      //POST batchlistbytes into validator 
      let resp =await fetch('http://rest-api:8008/batches', {
        method: 'POST',
             headers: {
         'Content-Type': 'application/octet-stream'
             },
             body: batchListBytes
              })
                console.log("response", resp);
    
          }
           catch(error) {
             console.log("error in fetch", error);
             return 'unauthorised';

           
         } 
     }
    }
    
    

}module.exports.eatfreshfoodClient = eatfreshfoodClient;
