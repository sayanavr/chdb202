var express = require('express');
var router = express.Router();
var {eatfreshfoodClient}  = require('./eatfreshfoodClient');
var fs = require('fs');
var bodyParser = require('body-parser');



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('dashboards', { title: 'Dashboards' });
});

router.post('/',function(req, res){
  var data = req.body.write;
  var pid = req.body.pid;
  var pvt = req.body.pvt;
  var sndr = req.body.sender;
  var role= req.body.role;

  console.log("Data sent to REST API", data);
  var client = new eatfreshfoodClient(pid,pvt,sndr,role);
  console.log('client value is '+client.msg);

  if(client.msg=='err')
    {
      res.send({message: "Unauthorised Access! Either userid or pvt key is wrong!"});
    }
  
  else
  {
  client.send_data([data]);
  res.send({message: "Data " +data+" successfully added"});
  }
  
})

router.get('/state/:data',async function(req,res){
  var pid = req.params.data;
  var client = new eatfreshfoodClient(pid,"Read","Read","R");
  var getData = client._send_to_rest_api(null);
  console.log("Data got from REST API", getData);
  getData.then(result => {res.send({ balance : result });});
})

module.exports = router;