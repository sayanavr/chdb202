const {TextEncoder, TextDecoder} = require('text-encoding/lib/encoding')
const { TransactionHandler } = require('sawtooth-sdk/processor/handler')
const crypto = require('crypto');

var encoder = new TextEncoder('utf8')
var decoder = new TextDecoder('utf8')

FAMILY_NAME='eatfreshfood';
const NAMESPACE = hash(FAMILY_NAME).substring(0, 6);

function hash(v) {
    return crypto.createHash('sha512').update(v).digest('hex');
}

function writeToStore(context, address, msg){
    let msgBytes = encoder.encode(msg);
    let entries = {
        [address]: msgBytes 
      }
    return context.setState(entries);
}

class eatfreshfoodHandler extends TransactionHandler{
    constructor(){
        super(FAMILY_NAME, ['1.0'], [NAMESPACE]);
    }

    apply(transactionProcessRequest, context){
        var msg = decoder.decode(transactionProcessRequest.payload);
        let header = transactionProcessRequest.header
        this.publicKey = header.signerPublicKey
        console.log('msg ====================================' + msg);
        var result = msg.split(" ");
        console.log('pid in tp is '+result[1]);
        var pid=result[1];
        this.address=hash("eatfreshDb").substr(0,6)+hash("thisisarandomtextusedtojustgeneratetheaddresshereinmyeatfreshcode").substr(0,60)+hash(pid).slice(0,4);

        return context.getState([this.address]).then((outcome)=>{
            console.log('outcome is'+typeof(outcome));
            var outString=JSON.stringify(outcome[this.address]);
            var outStringArray=msg.split(" ");
            console.log('outstr is '+outStringArray);
            console.log('outstr[0] is '+outStringArray[0]);
        
            if(outStringArray[0]=='D'){
                if(outcome[this.address]==''){
                    var newres='';
                }
                else
                {
                var newres=outcome[this.address]+' '+msg;
                }
                
            }
            else{
                var newres=msg;
                
            }

        return writeToStore(context, this.address, newres);
        })
    }
}
module.exports = eatfreshfoodHandler;