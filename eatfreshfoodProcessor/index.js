

'use strict'
//works in strict mode
const { TransactionProcessor } = require('sawtooth-sdk/processor')
//requires the module specified in ().
const eatfreshfoodHandler = require('./eatfreshfoodHandler');

if (process.argv.length < 3) {
  console.log('missing a validator address')
  process.exit(1)
}

const address = process.argv[2];
console.log('Connected---------------------------------------------------------------------');


const transactionProcessor = new TransactionProcessor(address)

transactionProcessor.addHandler(new eatfreshfoodHandler())


transactionProcessor.start()

